#!/usr/bin/env python3
# TODO: Integrate on feed-cantilever
import copy
from collections import defaultdict
from pathlib import Path
from dataclasses import dataclass, field
from datetime import datetime

import xml.etree.ElementTree as ET
import urllib.parse
import urllib.request

import argparse
import logging

logger = logging.getLogger(__name__)


def Url(string):
    try:
        parsed = urllib.parse.urlparse(string)
        return parsed.geturl()
    except ValueError:
        raise argparse.ArgumentTypeError(f'Invalid URL: {string}')


@dataclass
class Item:
    element: ET.Element
    guid: str = field(init=False)

    def __post_init__(self):
        guid = self.element.find('guid')
        self.guid = ''.join(guid.itertext())


def file_get_items(fobj):
    xml_root = ET.parse(fobj)
    return [Item(i) for i in xml_root.findall('./channel/item')]


def main(*args):
    parser = argparse.ArgumentParser(description='Split up feeds by categories')
    parser.add_argument('url', type=Url,
                        help='The URL to fetch')
    parser.add_argument('outfolder', type=Path,
                        help='The folder to output the split feeds')
    parser.add_argument('--max-items', type=int, default=50,
                        help='The maximum amount of items to output. Defaults to %(default)s')
    parser.add_argument('-v', '--verbose', dest='loglevel',
                        action='store_const', default=logging.INFO, const=logging.DEBUG,
                        help='Show detailed debug information')
    args = parser.parse_args(args)

    logging.basicConfig(level=args.loglevel, format='%(levelname)-5.5s %(name)s@%(funcName)s| %(message)s')

    logger.info('URL: %s', args.url)
    xml = None
    with urllib.request.urlopen(args.url) as response:
        xml = response.read()
    if xml is None:
        parser.error('Invalid URL contents')

    # XML Parsing
    xml_root = ET.fromstring(xml)
    xml_channel = xml_root.find('channel')  # Not sure if there's any real-world case of multiple channels

    # All items
    cat_items = defaultdict(list)
    for item in xml_channel.findall('item'):
        # item_title = ''.join(item.find('title').itertext())
        # logger.debug('=> %s', item_title)
        for item_category in item.findall('category'):
            item_category_name = ''.join(item_category.itertext())
            # logger.debug('   => %s', item_category_name)
            cat_items[item_category_name].append(Item(item))
        # Uncategorized items are lost
        xml_channel.remove(item)

    logger.info('- %d items', sum(map(len, cat_items.values())))
    logger.info('- %d categories', len(cat_items))

    # XML Root without all the "items"
    common_root = ET.ElementTree(xml_root)

    count_items = 0
    for cat, items in sorted(cat_items.items(), key=lambda tpl: len(tpl[1])):
        cat_quoted = urllib.parse.quote_plus(cat)
        logger.debug('=> %s [%d]' % (cat, len(items)))
        nfeed_root = copy.deepcopy(common_root)
        nfeed_title = nfeed_root.find('./channel/title')
        # Replace the title
        nfeed_title.text = f'{nfeed_title.text} | Category "{cat}"'
        # TODO: Replace the url? Needs a baseurl for the outfolder
        nfeed_file = args.outfolder / f'{cat_quoted}.xml'
        if nfeed_file.exists():
            old_items = file_get_items(nfeed_file.open())
            logger.debug('   %d existing items' % len(old_items))
            new_guids = [i.guid for i in items]
            filtered_old_items = [i for i in old_items if i.guid not in new_guids]
            logger.debug('   %d filtered items (old)' % len(filtered_old_items))
        else:
            filtered_old_items = []
        nfeed_channel = nfeed_root.find('channel')
        all_items = [*filtered_old_items, *items][:args.max_items]
        logger.debug('   %d items' % len(all_items))
        count_items += len(all_items)
        for item in all_items:
            # item_title = ''.join(item.element.find('title').itertext())
            # logger.debug('  => %s' % item_title)
            nfeed_channel.append(item.element)
        nfeed_root.write(nfeed_file, xml_declaration=True, encoding='unicode')

    logger.info('- Wrote %d items', count_items)

    # Write an HTML list
    # TODO: Migrate to jinja
    # TODO: Show table: count, last updated.
    # TODO: Sortable with JS: https://stackoverflow.com/a/49041392
    outfolder_index = args.outfolder / 'index.html'
    all_categories = {}  # fname: category
    for cat_file in args.outfolder.glob('*.xml'):
        cat_quoted = cat_file.stem
        all_categories[urllib.parse.quote(cat_file.name)] = (
            urllib.parse.unquote_plus(cat_quoted),
            datetime.fromtimestamp(cat_file.stat().st_mtime).isoformat(),
        )
    logger.info('- Indexing %d categories', len(all_categories))
    # Sort categories by date
    all_categories_txt = sorted(all_categories.items(), key=lambda tpl: tpl[1][1], reverse=True)
    outfolder_index.write_text(f'''
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Categories @ {args.outfolder.name}</title>
    </head>
    <body>
        <h1>Categories @ {args.outfolder.name}</h1>
        <p><b>URL</b>: <a href="{args.url}">{args.url}</a></p>
        <ul>
            {''.join(
                f"""
                <li>
                    <a href="{cfname}">{cname}</a>
                    Updated @
                    <span class="date">{cfdate}</date>
                </li>
                """
                for cfname, (cname, cfdate) in all_categories_txt)
            }
        </ul>
    </body>
    </html>
    ''')
    logger.info('Index: %s', outfolder_index.resolve())
    return True


def entrypoint():
    import sys
    result = main(*sys.argv[1:])
    return 0 if result else 1


if __name__ == '__main__':
    entrypoint()
