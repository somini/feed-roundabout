.PHONY: test test-samples
test: test-samples

test-samples: test-rps test-publico

test-%: tests/sample-%.xml
	mkdir -p tests/results/$*
	python3 feed-roundabout.py "file://$(PWD)/$<" tests/results/$*/

.PHONY: get-samples
get-samples: tests/sample-rps.xml tests/sample-publico.xml

tests/sample-rps.xml:
	# Sample: RockPaperShotgun
	curl -L "https://www.rockpapershotgun.com/feed" | xmllint -format - >"$@"

tests/sample-publico.xml:
	# Sample Público
	curl -L "https://feeds.feedburner.com/PublicoRSS" | xmllint -format - >"$@"
