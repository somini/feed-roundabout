# Feed Roundabout

Split a feed into multiple sub-feeds, and output those into a folder.

Currently supports only splitting by category.

NOTE: Items without category are discarded.

## Usage

Check the help on the entrypoint:

```sh
python3 feed-roundabout.py --help
```

To run a testcase, use:

```sh
make test
```

This will write some feeds to `tests/results/`.

## URL Encoding

On JavaScript, use `encodeURIComponent`
